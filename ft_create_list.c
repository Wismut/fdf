/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_list.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 18:37:22 by mivanov           #+#    #+#             */
/*   Updated: 2017/03/05 18:38:50 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_original		*get_new_original_list(void)
{
	t_original	*list;

	list = (t_original *)malloc(sizeof(t_original));
	list->next = NULL;
	list->coordinates = NULL;
	return (list);
}

t_projection	*get_new_projection_list(void)
{
	t_projection	*list;

	list = (t_projection *)malloc(sizeof(t_projection));
	list->coordinates = NULL;
	list->next = NULL;
	return (list);
}

t_mlxwin		*get_new_mlxwin_list(void)
{
	t_mlxwin	*list;

	list = (t_mlxwin *)malloc(sizeof(t_mlxwin));
	list->ptr_mlx = mlx_init();
	list->ptr_win = mlx_new_window(list->ptr_mlx, 1200, 1200, "FDF");
	return (list);
}

t_general		*get_new_general_list(void)
{
	t_general	*list;

	list = (t_general *)malloc(sizeof(t_general));
	list->projection = NULL;
	list->original = NULL;
	list->shift = 0;
	list->size = 0;
	list->mlxwin = NULL;
	return (list);
}
