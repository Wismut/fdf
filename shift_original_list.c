/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shift_original_list.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 18:33:03 by mivanov           #+#    #+#             */
/*   Updated: 2017/03/05 18:34:45 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_original	*change_coordinates(t_original *list, int shift_x, int shift_y)
{
	int			i;
	t_original	*head;

	head = list;
	while (list->next)
	{
		i = 0;
		while (list->coordinates[i])
		{
			list->coordinates[i][0] = list->coordinates[i][0] - shift_y;
			list->coordinates[i][2] = list->coordinates[i][2] - shift_x;
			i++;
		}
		list = list->next;
	}
	return (head);
}

int			get_max_height(t_original *list)
{
	int		i;

	i = 0;
	while (list->coordinates[i])
		i++;
	return (i);
}

int			get_max_lenght(t_original *list)
{
	int		i;

	i = 0;
	while (list)
	{
		i++;
		list = list->next;
	}
	return (i);
}

t_original	*update_original_size(t_original *list)
{
	t_original	*head;
	int			count_x;
	int			count_y;
	float		size;

	head = list;
	count_x = get_max_lenght(list);
	count_y = get_max_height(list);
	size = 800.0f / (count_x < count_y ? count_y : count_x);
	return (head);
}

t_original	*shift_center_to_begin(t_original *list)
{
	t_original	*head;
	int			count_x;
	int			count_y;

	head = list;
	count_x = get_max_lenght(list);
	count_y = get_max_height(list);
	head = change_coordinates(list, count_x / 2, count_y / 2);
	return (head);
}
