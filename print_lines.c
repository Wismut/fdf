/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_lines.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 20:22:15 by mivanov           #+#    #+#             */
/*   Updated: 2017/03/05 22:11:06 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void			print_vertical_line(t_projection *t, void *mlx, void *win)
{
	t_projection	*cur;
	int				j;

	cur = t;
	j = 0;
	while (cur->coordinates[j] && cur->coordinates[j + 1])
	{
		draw_line(cur->coordinates[j], cur->coordinates[j + 1], mlx, win);
		j++;
	}
}

t_projection	*print_horizontal_lines(t_projection *t, void *mlx, void *win)
{
	int				i;
	t_projection	*head;

	head = t;
	while (t->next)
	{
		i = 0;
		while (t->next->next && t->coordinates[i] && t->next->coordinates[i])
		{
			draw_line(t->coordinates[i], t->next->coordinates[i], mlx, win);
			i++;
		}
		t = t->next;
	}
	return (head);
}

void			print_vertical_lines(t_projection *t, void *mlx, void *win)
{
	while (t->next)
	{
		print_vertical_line(t, mlx, win);
		t = t->next;
	}
}
