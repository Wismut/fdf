/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/17 18:26:57 by mivanov           #+#    #+#             */
/*   Updated: 2016/12/26 19:45:04 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

char	*ft_strnjoin(char const *s1, char const *s2, int n)
{
	char	*str;

	if (!s1 || !s2)
		return (NULL);
	str = (char *)ft_memalloc(ft_strlen(s1) + 1 + n);
	if (str)
	{
		ft_strcat(str, s1);
		ft_strncat(str, s2, n);
	}
	return (str);
}

char	*ft_strndup(const char *s1, int n)
{
	size_t	i;
	char	*str;

	i = 0;
	while (s1[i])
		i++;
	str = (char*)ft_memalloc((i + 1) * sizeof(*str));
	if (!str)
		return (NULL);
	i = 0;
	while (s1[i] && n > 0)
	{
		str[i] = s1[i];
		i++;
		n--;
	}
	str[i] = '\0';
	return (str);
}

t_list	*ft_correct_fd(t_list **head, size_t fd)
{
	t_list	*node;

	node = *head;
	while (node)
	{
		if (fd == node->content_size)
			return (node);
		node = node->next;
	}
	node = ft_lstnew("\0", 1);
	node->content_size = fd;
	ft_lstadd(head, node);
	return (node);
}

int		get_next_line(const int fd, char **line)
{
	static	t_list	*fd_list;
	t_list			*head;
	char			buff[BUFF_SIZE + 1];
	int				result;
	char			*p;

	if (fd < 0 || read(fd, buff, 0) < 0 || !line)
		return (-1);
	head = fd_list;
	fd_list = ft_correct_fd(&head, fd);
	while (!ft_strchr(fd_list->content, '\n' &&
					(result = read(fd, buff, BUFF_SIZE))))
		fd_list->content = ft_strnjoin(fd_list->content, buff, result);
	result = 0;
	while (((char *)fd_list->content)[result] &&
			((char *)fd_list->content)[result] != '\n')
		result++;
	*line = ft_strndup(fd_list->content, result);
	if (((char *)fd_list->content)[result] == '\n')
		result++;
	p = fd_list->content;
	fd_list->content = ft_strdup(fd_list->content + result);
	free(p);
	fd_list = head;
	return (result ? 1 : 0);
}
