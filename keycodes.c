/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keycodes.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 12:57:49 by mivanov           #+#    #+#             */
/*   Updated: 2017/03/06 13:00:44 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	keycode_left(t_general *general)
{
	mlx_clear_window(general->mlxwin->ptr_mlx, general->mlxwin->ptr_win);
	general->original = move_figure_left_right(general->original, -20);
	print_projection(general, 0);
}

void	keycode_right(t_general *general)
{
	mlx_clear_window(general->mlxwin->ptr_mlx, general->mlxwin->ptr_win);
	general->original = move_figure_left_right(general->original, 20);
	print_projection(general, 0);
}

void	keycode_down(t_general *general)
{
	mlx_clear_window(general->mlxwin->ptr_mlx, general->mlxwin->ptr_win);
	general->original = move_figure_up_down(general->original, 20);
	print_projection(general, 0);
}

void	keycode_up(t_general *general)
{
	mlx_clear_window(general->mlxwin->ptr_mlx, general->mlxwin->ptr_win);
	general->original = move_figure_up_down(general->original, -20);
	print_projection(general, 0);
}
