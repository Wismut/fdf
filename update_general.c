/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update_general.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 19:51:13 by mivanov           #+#    #+#             */
/*   Updated: 2017/03/05 19:51:58 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_general	*update_general(t_original *original, t_projection *projection,
t_general *general, int size)
{
	t_general	*head;

	head = general;
	head->projection = projection;
	head->original = original;
	head->size = size;
	return (head);
}

t_general	*update_general2(t_general *general, t_mlxwin *mlxwin, int shift)
{
	t_general	*head;

	head = general;
	head->mlxwin = mlxwin;
	head->shift = shift;
	return (general);
}
