/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 19:52:33 by mivanov           #+#    #+#             */
/*   Updated: 2017/03/20 14:34:20 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		get_size_of_line(t_original *list)
{
	int		size;

	size = 0;
	while (list->coordinates[size])
		size++;
	return (size);
}

void	is_list_valid(t_original *list)
{
	int		i;

	if (!list)
		ft_error("Not valid file\n");
	i = get_size_of_line(list);
	while (list && list->coordinates)
	{
		if (i != get_size_of_line(list))
			ft_error("Not valid file\n");
		list = list->next;
	}
}

int		key_hook(int keycode, t_general *general)
{
	if (keycode == 53)
		exit(0);
	else if (keycode == 123)
		keycode_left(general);
	else if (keycode == 124)
		keycode_right(general);
	else if (keycode == 125)
		keycode_down(general);
	else if (keycode == 126)
		keycode_up(general);
	else if (keycode == 69)
	{
		mlx_clear_window(general->mlxwin->ptr_mlx, general->mlxwin->ptr_win);
		print_projection(general, 1);
	}
	else if (keycode == 78)
	{
		mlx_clear_window(general->mlxwin->ptr_mlx, general->mlxwin->ptr_win);
		print_projection(general, 2);
	}
	return (1);
}

void	create_original_list(t_original **original_list,
t_projection **projection_list, char *argv, int i)
{
	int			k;
	int			fd;
	char		*string;
	float		**coordinates;
	t_original	*head;

	head = *original_list;
	fd = open(argv, O_RDONLY);
	while ((k = get_next_line(fd, &string)) > 0)
	{
		coordinates = get_float_array(ft_split(string), i);
		(*original_list)->coordinates = coordinates;
		(*original_list)->next = get_new_original_list();
		*original_list = (*original_list)->next;
		(*projection_list)->coordinates = get_projection_coords(coordinates);
		(*projection_list)->next = get_new_projection_list();
		*projection_list = (*projection_list)->next;
		i++;
	}
	k == -1 || i == 0 ? ft_error("Not valid file\n") : 0;
	is_list_valid(head);
	(*projection_list)->next = NULL;
	(*original_list)->next = NULL;
}

int		main(int argc, char **argv)
{
	t_original		*original_list;
	t_projection	*projection_head;
	t_projection	*projection_list;
	t_mlxwin		*mlxwin;
	t_general		*general;

	if (argc != 2)
		ft_error("Incorrect count of arguments\n");
	general = get_new_general_list();
	general->original = get_new_original_list();
	projection_head = get_new_projection_list();
	original_list = general->original;
	projection_list = projection_head;
	create_original_list(&original_list, &projection_list, argv[1], 0);
	mlxwin = get_new_mlxwin_list();
	general->original = shift_center_to_begin(general->original);
	general = update_general(general->original, projection_head, general, 1);
	general = update_general2(general, mlxwin, 0);
	general->projection = create_projection(general->original,
general->projection, (float)(M_PI * 25 / 180), (float)(M_PI * 115 / 180));
	general->projection = update_projection(general->projection, 100, 600);
	general->size = 1;
	print_projection(general, 0);
	mlx_hook(mlxwin->ptr_win, 2, 5, key_hook, general);
	mlx_loop(mlxwin->ptr_mlx);
}
