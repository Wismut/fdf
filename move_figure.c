/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_figure.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 19:49:34 by mivanov           #+#    #+#             */
/*   Updated: 2017/03/05 19:50:06 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_original	*move_figure_left_right(t_original *list, int shift)
{
	t_original	*head;
	int			i;

	head = list;
	while (list->next)
	{
		i = 0;
		while (list->coordinates[i])
		{
			list->coordinates[i][0] = list->coordinates[i][0] + shift;
			i++;
		}
		list = list->next;
	}
	return (head);
}

t_original	*move_figure_up_down(t_original *list, int shift)
{
	t_original	*head;
	int			i;

	head = list;
	while (list->next)
	{
		i = 0;
		while (list->coordinates[i])
		{
			list->coordinates[i][1] = list->coordinates[i][1] + shift;
			i++;
		}
		list = list->next;
	}
	return (head);
}
