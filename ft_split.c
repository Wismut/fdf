/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 19:43:26 by mivanov           #+#    #+#             */
/*   Updated: 2017/03/05 19:48:46 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

char		**get_char_array(char *str, int k, int count, char **result)
{
	int		i;
	int		j;

	j = 0;
	i = 0;
	while (str[i])
	{
		while (str[i] == ' ' || str[i] == '\t' || str[i] == '\n')
			i++;
		j = i;
		while (str[j] != ' ' && str[j] != '\t' && str[j] != '\n' && str[j])
			j++;
		if (i == j)
			break ;
		result[count] = (char *)malloc(j - i + 1);
		k = j - i;
		j = 0;
		while (k-- > 0)
			result[count][j++] = str[i++];
		result[count++][j] = '\0';
	}
	result[count] = NULL;
	return (result);
}

char		**ft_split(char *str)
{
	int		i;
	int		count;
	char	**result;

	i = 1;
	count = 0;
	if (!str)
		return (NULL);
	while (str[i - 1])
	{
		if ((str[i - 1] == ' ' || str[i - 1] == '\n' || str[i - 1] == '\t') &&
			(str[i] != ' ' && str[i] != '\t' && str[i] != '\n'))
			count++;
		i++;
	}
	result = (char **)malloc(sizeof(char *) * (count + 1));
	count = 0;
	result = get_char_array(str, 0, count, result);
	return (result);
}
