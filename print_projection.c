/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_projection.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 18:28:05 by mivanov           #+#    #+#             */
/*   Updated: 2017/03/05 18:32:30 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	print_projection(t_general *general, int n)
{
	t_general	*head;

	head = general;
	(n == 1) ? (general->size = general->size + 0.5f) : 0;
	(n == 2) ? (general->size = general->size - 0.5f) : 0;
	general->projection = create_projection(general->original,
general->projection, (float)(M_PI * 45 / 180), (float)(M_PI * 135 / 180));
	general->projection = update_projection(general->projection, general->size,
600);
	print_vertical_lines(general->projection, general->mlxwin->ptr_mlx,
general->mlxwin->ptr_win);
	general->projection = create_projection(general->original, head->projection,
(float)(M_PI * 45 / 180), (float)(M_PI * 135 / 180));
	general->projection = update_projection(general->projection, general->size,
600);
	print_horizontal_lines(general->projection, general->mlxwin->ptr_mlx,
general->mlxwin->ptr_win);
}
