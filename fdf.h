/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 18:43:41 by mivanov           #+#    #+#             */
/*   Updated: 2017/03/10 18:05:32 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_FDF_H
# define FDF_FDF_H

# include <fcntl.h>
# include <unistd.h>
# include <string.h>
# include <stdlib.h>
# include <math.h>
# include <mlx.h>
# include "libft/libft.h"
# include "libft/get_next_line.h"
# define MAX 1200

typedef struct			s_original
{
	float				**coordinates;
	struct s_original	*next;
}						t_original;

typedef struct			s_projection
{
	float				**coordinates;
	struct s_projection	*next;
}						t_projection;

typedef struct			s_mlxwin
{
	void				*ptr_mlx;
	void				*ptr_win;
}						t_mlxwin;

typedef struct			s_general
{
	t_projection		*projection;
	t_original			*original;
	int					shift;
	float				size;
	t_mlxwin			*mlxwin;
}						t_general;

void					keycode_left(t_general *general);
void					keycode_right(t_general *general);
void					keycode_down(t_general *general);
void					keycode_up(t_general *general);
void					ft_error(char *error);
void					draw_line(float *cur, float *next, void *mlx,
void *win);
int						get_next_line(const int fd, char **line);
t_original				*get_new_original_list();
char					**ft_split(char *str);
int						get_coordinate(char *str);
t_projection			*get_new_projection_list();
float					**get_float_array(char **str, int z);
t_projection			*print_horizontal_lines(t_projection *t, void *mlx,
void *win);
void					print_vertical_lines(t_projection *t, void *mlx,
void *win);
float					**get_projection_coords(float **o);
t_projection			*create_projection(t_original *original,
t_projection *projection, float alpha, float beta);
t_projection			*update_projection(t_projection *list,
float size, int shift);
void					draw_line(float *cur_array,
float *next_array, void *mlx, void *win);
t_mlxwin				*get_new_mlxwin_list();
t_original				*shift_center_to_begin(t_original *list);
void					print_projection(t_general *general, int n);
t_original				*move_figure_left_right(t_original *list, int shift);
t_original				*move_figure_up_down(t_original *list, int shift);
t_general				*get_new_general_list();
t_general				*update_general(t_original *original,
t_projection *projection, t_general *general, int size);
t_general				*update_general2(t_general *general, t_mlxwin *mlxwin,
int shift);
void					create_original_list(t_original **original_list,
t_projection **projection_list, char *argv, int i);
t_projection			*increase_size_figure(t_projection *projection);
t_projection			*update_projection2(t_projection *projection);

#endif
