/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   update_size_figure.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 18:56:44 by mivanov           #+#    #+#             */
/*   Updated: 2017/03/05 18:56:45 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_projection	*increase_size_figure(t_projection *projection)
{
	t_projection	*head;
	int				i;

	head = projection;
	while (projection->next)
	{
		i = 0;
		while (projection->coordinates[i])
		{
			projection->coordinates[i][0] *= 2;
			projection->coordinates[i][1] *= 2;
			projection->coordinates[i][2] *= 2;
			i++;
		}
		projection = projection->next;
	}
	return (head);
}

t_original		*reduce_size_figure(t_original *original)
{
	t_original	*head;
	int			i;

	head = original;
	while (original->next)
	{
		i = 0;
		while (original->coordinates[i])
		{
			original->coordinates[i][0] *= 0.5;
			original->coordinates[i][1] *= 0.5;
			original->coordinates[i][2] *= 0.5;
			i++;
		}
		original = original->next;
	}
	return (head);
}
