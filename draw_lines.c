/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_lines.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 22:00:28 by mivanov           #+#    #+#             */
/*   Updated: 2017/03/10 17:41:33 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void			change_y(float *cur, int *error, int *deltas, int *signs)
{
	*error -= deltas[1];
	cur[1] += signs[0];
}

void			change_x(float *cur, int *error, int *deltas, int *signs)
{
	*error += deltas[0];
	cur[0] += signs[1];
}

void			draw_line(float *cur, float *next, void *mlx, void *win)
{
	int		error;
	int		error2;
	int		deltas[2];
	int		signs[2];

	deltas[0] = (float)fabs((float)((int)next[1] - (int)cur[1]));
	deltas[1] = (double)fabs((double)((int)next[0] - (int)cur[0]));
	signs[0] = (int)cur[1] < (int)next[1] ? 1 : -1;
	signs[1] = (int)cur[0] < (int)next[0] ? 1 : -1;
	error = deltas[0] - deltas[1];
	if ((int)next[1] > 0 && (int)next[0] > 0)
		mlx_pixel_put(mlx, win, (int)next[1], (int)next[0], 0xFFFFFF);
	while (((int)cur[0] != (int)next[0] || (int)cur[1] != (int)next[1]) &&
((int)cur[0] > 0 && (int)cur[1] > 0 && (int)cur[0] < MAX && (int)cur[1] < MAX))
	{
		mlx_pixel_put(mlx, win, (int)cur[1], (int)cur[0], 0xFFFFFF);
		error2 = error * 2;
		error2 > -deltas[1] ? change_y(cur, &error, deltas, signs) : 0;
		if (error2 < deltas[0])
			change_x(cur, &error, deltas, signs);
		if (error2 <= -deltas[1] && error2 >= deltas[0])
			return ;
	}
}
