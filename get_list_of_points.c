/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_list_of_points.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 18:41:01 by mivanov           #+#    #+#             */
/*   Updated: 2017/03/05 18:42:20 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		get_coordinate(char *str)
{
	int		i;
	int		sign;
	int		res;

	res = 0;
	i = 0;
	if (!str)
		return (0);
	sign = str[i] == '-' ? -1 : 1;
	i = str[i] == '-' ? 1 : 0;
	while (str[i] && str[i] != ',')
		res = res * 10 + (str[i++] - '0');
	return (res * sign);
}

float	**get_float_array(char **str, int z)
{
	int		temp;
	int		x;
	float	**res;

	x = 0;
	while (str[x])
		x++;
	res = (float **)malloc(sizeof(float *) * x + 1);
	temp = x;
	x = 0;
	while (x < temp)
	{
		res[x] = (float *)malloc(sizeof(float) * 3);
		res[x][0] = (float)x;
		res[x][1] = (float)-get_coordinate(str[x]);
		res[x][2] = (float)z;
		x++;
	}
	res[x] = NULL;
	return (res);
}
