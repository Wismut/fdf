#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mivanov <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/05 15:25:00 by mivanov           #+#    #+#              #
#    Updated: 2017/03/06 13:04:56 by mivanov          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = fdf

G = gcc

FLAG = -Wall -Wextra -Werror

LIB = libft.a

SRC = create_projection.c \
		ft_create_list.c \
		ft_split.c \
		get_list_of_points.c \
		main.c \
		move_figure.c \
		print_lines.c \
		print_projection.c \
		shift_original_list.c \
		update_general.c \
		update_size_figure.c \
		draw_lines.c \
		ft_error.c \
		keycodes.c \

OBJ = $(SRC:.c=.o)

LIBFTDIR = libft

all: $(NAME)

$(NAME) : $(OBJ) $(LIB)
			$(G) $(FLAG) $(OBJ) -o $(NAME) libft/$(LIB) -o $(NAME) -lmlx -framework OpenGL -framework AppKit
%.o: %.c
			$(G) $(FLAG) -c $<

$(LIB) :
		make -C libft/

cleanlib:
		make -C libft/ clean

fcleanlib:
		make -C libft/ fclean

clean: cleanlib
			rm -f *.o

fclean: clean fcleanlib
			rm -f $(NAME)

re: fclean all