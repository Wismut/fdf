/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_projection.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mivanov <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/05 18:57:25 by mivanov           #+#    #+#             */
/*   Updated: 2017/03/10 17:06:30 by mivanov          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_projection	*create_projection(t_original *original,
t_projection *projection, float alpha, float beta)
{
	int				i;
	t_projection	*head;

	head = projection;
	while (original->next)
	{
		i = 0;
		while (original->coordinates[i])
		{
			projection->coordinates[i][1] =
					-original->coordinates[i][0] * cos(beta) -
					original->coordinates[i][2] * sin(beta);
			projection->coordinates[i][0] =
					original->coordinates[i][0] * sin(alpha) * cos(alpha) +
					original->coordinates[i][1] * cos(alpha) -
					original->coordinates[i][2] * sin(alpha) * cos(beta);
			i++;
		}
		original = original->next;
		projection = projection->next;
	}
	return (head);
}

t_projection	*update_projection(t_projection *list, float size, int shift)
{
	t_projection	*head;
	int				i;

	head = list;
	while (list->next)
	{
		i = 0;
		while (list->coordinates[i])
		{
			list->coordinates[i][0] = list->coordinates[i][0] * size + shift;
			list->coordinates[i][1] = list->coordinates[i][1] * size + shift;
			i++;
		}
		list = list->next;
	}
	return (head);
}

float			**get_projection_coords(float **original_coordinates)
{
	int		i;
	float	**result;

	i = 0;
	while (original_coordinates[i])
		i++;
	result = (float **)malloc(sizeof(float *) * (i + 1));
	i = 0;
	while (original_coordinates[i])
	{
		result[i] = (float *)malloc(sizeof(float) * 2);
		result[i][0] = original_coordinates[i][0];
		result[i][1] = original_coordinates[i][1];
		i++;
	}
	result[i] = NULL;
	return (result);
}
